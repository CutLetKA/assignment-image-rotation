#include "../include/bmp.h"
#include "../include/files.h"
#include "../include/image.h"
#include "../include/transformations.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

static const char* const bmp_error_messages[] = {
        "Error: The proposed file does not correspond to the bmp format\n",
        "Error: An error occurred when working with the bmp format\n"
};

static const char* const files_error_messages[] = {
        "Error: no such file\n",
        "Error: permission denied\n",
        "Error: the file cannot be closed\n",
        "Error: the file is corrupted\n"
};

bool file_status_check (enum file_status file_status) {
    if (file_status != FILE_SUCCESS) {
        fputs(files_error_messages[file_status], stderr);
        return 1;
    }
    return 0;
}

bool bmp_status_check (enum bmp_status bmp_status) {
    if (bmp_status != BMP_SUCCESS) {
        fputs(bmp_error_messages[bmp_status], stderr);
        return 1;
    }
    return 0;
}



int main(int argc, char* argv[]) {
    if (argc != 3) {
        fprintf(stderr, "Usage: ./image-transformer <source-image> <transformed-image>");
        return 1;
    }

    FILE* file;
    enum file_status file_status = file_open(&file, argv[1], "rb");

    if (file_status_check(file_status)) {
        return 1;
    }

    struct image source = {0};
    enum bmp_status bmp_status = from_bmp(file, &source);
    file_status = file_close(&file);

    if (file_status_check(file_status)){
        return 1;
    }

    if (bmp_status_check(bmp_status)) {
        image_destroy(&source);
        return 1;
    }

    struct image result = rotate(source);
    image_destroy(&source);

    file_status = file_open(&file, argv[2], "wb");
    
    if (file_status_check(file_status)) {
        image_destroy(&result);
        return 1;
    }

    bmp_status = to_bmp(file, result);

    file_status = file_close(&file);

    if (file_status_check(file_status)){
        return 1;
    }

    image_destroy(&result);

    if (bmp_status_check(bmp_status)) {
        return 1;
    }

    fputs("Success! Check rotated image\n", stderr);
    return 0;
}

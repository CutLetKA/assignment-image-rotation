#include "../include/files.h"
#include <errno.h>

enum file_status file_close(FILE** file){
    if (*file) {
        if (fclose(*file)) {
            return FILE_CLOSE_ERROR;
        }
        return FILE_SUCCESS;
    }
    return FILE_NOT_EXIST;
}

enum file_status file_open(FILE** file, const char* name, const char* mode) {
    *file = fopen(name, mode);
    if (*file != NULL) return FILE_SUCCESS;
    if (errno == ENOENT) return FILE_NOT_EXIST;
    if (errno == EACCES) return FILE_PERMISSION_DENIED;
    return FILE_ERROR;
}

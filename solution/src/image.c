#include "../include/image.h"
#include <malloc.h>

struct pixel* image_find_pixel(uint32_t x, uint32_t y, struct image const* const img) {
    return img->data + x + y * img->width;
}

struct image image_create(uint64_t width, uint64_t height) {
    return (struct image) {width, height, malloc(sizeof(struct pixel) * width * height)};
}

void image_destroy(struct image* const img) {
    img->width = 0;
    img->height = 0;
    if (img->data != NULL) free(img->data);
}

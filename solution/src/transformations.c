#include "../include/transformations.h"

struct image rotate(struct image const image) {
    struct image new_image = image_create(image.height, image.width);
    for (uint32_t y = 0; y < image.height; y++)
        for (uint32_t x = 0; x < image.width; x++)
            *image_find_pixel(image.height - 1 - y, x, &new_image) = *image_find_pixel(x, y, &image);
    return new_image;
}

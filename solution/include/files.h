#ifndef FILES_H
#define FILES_H

#include <stdio.h>

enum file_status {
    FILE_NOT_EXIST = 0,
    FILE_PERMISSION_DENIED,
    FILE_CLOSE_ERROR,
    FILE_ERROR,
    FILE_SUCCESS
};
enum file_status file_close(FILE** file);
enum file_status file_open(FILE** file, const char* name, const char* mode);

#endif

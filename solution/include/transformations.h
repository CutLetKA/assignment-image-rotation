#ifndef TRANSFORMATIONS_H
#define TRANSFORMATIONS_H

#include "image.h"

struct image rotate(struct image const image);

#endif
